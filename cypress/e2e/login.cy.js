describe("This is the describe block", ()=> {
    it("This is an it block", ()=> {
        cy.visit("www.google.com")
        cy.get("h1").should('be.visible')
    })
})